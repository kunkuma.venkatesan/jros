This repository holds the necessary artifacts for building and running the JROS profile using mybinder.
To launch the profile, click the following icon
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fkunkuma.venkatesan%2Fjros/HEAD)


#### Setting up workspaces inside binder

To source new ROS workspaces, so that this can be fetched by jupyterlab-urd and zethus plugins, 
the workspace paths should be added using ROS -> Settings.

For example:

Add the following path "/home/jovyan/test_ws" to **Workspace Paths** available under *ROS -> Settings*, 
and double click on the file "anymal.urdf".

#### Preview
![pub_sub.png](./pub_sub.png)

#### Credits 
- https://robostack.github.io/
- https://github.com/ANYbotics/anymal_b_simple_description/
- https://github.com/RoboStack/jupyterlab-ros
- https://github.com/jupyter-robotics/jupyterlab-urdf