
To source new ros workspaces, so that it can be fetched by the 
jros, zethus jupyterlab plugins, the workspace paths should be
added using ROS -> Settings.

For example:

Add the following path "/home/jovyan/test_ws" to Workspace Paths 
available under ROS -> Settings, and double click on the file
"anymal.urdf".
